import {
    Button,
    FormControlLabel,
    FormGroup,
    Grid,
    MenuItem,
    Checkbox,
    TextField,
    Tooltip,
    Typography,
    Theme,
} from "@mui/material"
import { Field } from "@/types/types"
import React from "react"
import HelpIcon from "@mui/icons-material/Help"
import FileUploadIcon from "@mui/icons-material/FileUpload"

export class Input extends React.Component<{
    field: Field
    error: boolean
    defaultVal: string
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}> {
    render(): React.ReactNode {
        
        return (
            <Grid container direction="row" justifyContent="space-between" alignItems="center">
                <Grid container item xs={11.5} justifyContent="flex-start">
                    <TextField
                        required={this.props.field.isRequired}
                        label={this.props.field.fieldText}
                        variant="outlined"
                        error={this.props.error}
                        value={this.props.defaultVal}
                        fullWidth
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                            if (this) {
                                this.props.onChange(event)
                            }
                            event.preventDefault()
                        }}
                        helperText={this.props.error ? "Bitte füllen Sie dieses Feld aus" : ""}
                        sx={{ marginTop: "10px", marginBottom: "10px" }}
                    />
                </Grid>
                <Grid container item xs={0.3} justifyContent="flex-end">
                    {this.props.field.fieldDescription && (
                        <Tooltip title={this.props.field.fieldDescription}>
                            <HelpIcon sx={{ color: "gray" }}></HelpIcon>
                        </Tooltip>
                    )}
                </Grid>
            </Grid>
        )
    }
}

export class Dropdown extends React.Component<{
    error: boolean
    field: Field
    defaultVal: string
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}> {
    render(): React.ReactNode {
        return (
            <Grid container direction="row" justifyContent="space-between" alignItems="center">
                <Grid container item xs={11.5} justifyContent="flex-start">
                    <TextField
                        required={this.props.field.isRequired}
                        label={this.props.field.fieldText}
                        error={this.props.error}
                        variant="outlined"
                        value={this.props.defaultVal}
                        helperText={this.props.error ? "Bitte füllen Sie dieses Feld aus" : ""}
                        select
                        fullWidth
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                            if (this) {
                                this.props.onChange(event)
                            }
                            event.preventDefault()
                        }}
                        sx={{ marginTop: "10px", marginBottom: "10px" }}
                    >
                        {this.props.field.values ? (
                            this.props.field.values.map((value: string, index: number) => (
                                <MenuItem key={index} value={value}>
                                    {value}
                                </MenuItem>
                            ))
                        ) : (
                            <MenuItem>
                                <em>Keiner</em>
                            </MenuItem>
                        )}
                    </TextField>
                </Grid>
                <Grid container item xs={0.3} justifyContent="flex-end">
                    {this.props.field.fieldDescription && (
                        <Tooltip title={this.props.field.fieldDescription}>
                            <HelpIcon sx={{ color: "gray" }}></HelpIcon>
                        </Tooltip>
                    )}
                </Grid>
            </Grid>
        )
    }
}

export class File extends React.Component<{
    field: Field
    error: boolean
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}> {

    render(): React.ReactNode {
        return (
            <Grid container direction="row" justifyContent="space-between" alignItems="center" borderColor="error.main">
                <Grid container item direction="column" xs={12} justifyContent="flex-start">
                    <Grid item>
                        <Button
                            variant="contained"
                            component="label"
                            sx={{ marginTop: "10px", width: "auto" }}
                            startIcon={<FileUploadIcon />}
                            color={this.props.error ? "error" : "primary"}
                        >
                            Select {this.props.field.fieldText}{this.props.field.isRequired ? "*" : ""}
                            <input
                                type="file"
                                onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                                    if (this) {
                                        this.props.onChange(event)
                                    }
                                    event.preventDefault()
                                }}
                                hidden
                            />
                        </Button>
                    </Grid>
                    <Grid item>
                        <Typography sx={{ color: "gray", marginTop: "3px" }} variant="body2" noWrap>
                            {this.props.field.fieldDescription}
                        </Typography>
                    </Grid>
                </Grid>
            </Grid>
        )
    }
}

export class Num extends React.Component<{
    field: Field
    error: boolean
    defaultVal: string
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}> {
    render(): React.ReactNode {
        return (
            <Grid container direction="row" justifyContent="space-between" alignItems="center">
                <Grid container item xs={11.5} justifyContent="flex-start">
                    <TextField
                        required={this.props.field.isRequired}
                        label={this.props.field.fieldText}
                        value={this.props.defaultVal}
                        helperText={this.props.error ? "Bitte füllen Sie dieses Feld aus" : ""}
                        type="number"
                        fullWidth
                        error={this.props.error}
                        sx={{ marginTop: "10px", marginBottom: "10px" }}
                        variant="outlined"
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                            if (this) {
                                this.props.onChange(event)
                            }
                            event.preventDefault()
                        }}
                    />
                </Grid>
                <Grid container item xs={0.3} justifyContent="flex-end">
                    {this.props.field.fieldDescription && (
                        <Tooltip title={this.props.field.fieldDescription}>
                            <HelpIcon sx={{ color: "gray" }}></HelpIcon>
                        </Tooltip>
                    )}
                </Grid>
            </Grid>
        )
    }
}

export class Date extends React.Component<{
    field: Field
    error: boolean
    defaultVal: string
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}> {
    render(): React.ReactNode {
        return (
            <Grid container direction="row" justifyContent="space-between" alignItems="center">
                <Grid container item xs={11.5} justifyContent="flex-start">
                    <TextField
                        required={this.props.field.isRequired}
                        label={this.props.field.fieldText}
                        value={this.props.defaultVal}
                        helperText={this.props.error ? "Bitte füllen Sie dieses Feld aus" : ""}
                        type="date"
                        fullWidth
                        focused
                        error={this.props.error}
                        sx={{ marginTop: "10px", marginBottom: "10px" }}
                        variant="outlined"
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                            if (this) {
                                this.props.onChange(event)
                            }
                            event.preventDefault()
                        }}
                    />
                </Grid>
                <Grid container item xs={0.3} justifyContent="flex-end">
                    {this.props.field.fieldDescription && (
                        <Tooltip title={this.props.field.fieldDescription}>
                            <HelpIcon sx={{ color: "gray" }}></HelpIcon>
                        </Tooltip>
                    )}
                </Grid>
            </Grid>
        )
    }
}

export class LargeInput extends React.Component<{
    field: Field
    error: boolean
    defaultVal: string
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}> {
    render(): React.ReactNode {
        return (
            <Grid container direction="row" justifyContent="space-between" alignItems="center">
                <Grid container item xs={11} justifyContent="flex-start">
                    <TextField
                        required={this.props.field.isRequired}
                        label={this.props.field.fieldText}
                        value={this.props.defaultVal}
                        helperText={this.props.error ? "Bitte füllen Sie dieses Feld aus" : ""}
                        multiline
                        fullWidth
                        focused
                        error={this.props.error}
                        sx={{ marginTop: "10px", marginBottom: "10px" }}
                        variant="outlined"
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                            if (this) {
                                this.props.onChange(event)
                            }
                            event.preventDefault()
                        }}
                    />
                </Grid>
                <Grid container item xs={1} justifyContent="flex-end">
                    {this.props.field.fieldDescription && (
                        <Tooltip title={this.props.field.fieldDescription}>
                            <HelpIcon sx={{ color: "gray" }}></HelpIcon>
                        </Tooltip>
                    )}
                </Grid>
            </Grid>
        )
    }
}

export class Submit extends React.Component<{
    theme: Theme;
    onClick?: () => void;
  }> {
    render(): React.ReactNode {
      return (
        <Button
          variant="contained"
          style={{
            ...this.props.theme.typography,
            minWidth: "80px",
          }}
          onClick={this.props.onClick}
        >
          Abgeben
        </Button>
      );
    }
  }
