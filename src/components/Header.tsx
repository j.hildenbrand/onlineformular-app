import { AppBar, Box, Link, Stack, Toolbar, Typography } from "@mui/material"
import React from "react";

export class Header extends React.Component<{
    title: string
    theme: any
}> {
    render(): React.ReactNode {
        return (
            <>
                <AppBar
                    position="fixed"
                    sx={{
                        zIndex: this.props.theme.zIndex.drawer + 1,
                        transition: this.props.theme.transitions.create(["width", "margin"], {
                            easing: this.props.theme.transitions.easing.sharp,
                            duration: this.props.theme.transitions.duration.leavingScreen,
                        }),
                    }}
                >
                    <Toolbar
                        sx={{
                            // keeps right padding when drawer closed
                            pr: "24px",
                            // ...this.props.theme.mixins.toolbar,
                        }}
                    >
                        <Box sx={{ flexGrow: 1 }}>
                            <Stack direction="row" sx={{ alignItems: "center" }}>
                                <Link href="/">
                                    <Typography
                                        variant="h6"
                                        component="h1"
                                        color="#FFFFFF"
                                        noWrap
                                        sx={{
                                            fontWeight: this.props.theme.typography.fontWeightBold,
                                        }}
                                    >
                                        {this.props.title} Portal
                                    </Typography>
                                </Link>
                            </Stack>
                        </Box>
                    </Toolbar>
                </AppBar>
            </>
        )
    }
}

export default Header