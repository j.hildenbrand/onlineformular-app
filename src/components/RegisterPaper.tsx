import React from "react";
import {
  Paper as MUIPaper,
  Box,
  Typography,
  Button,
  CircularProgress,
  Grid,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";

type PaperProps = {
  mode: "register";
  handleAction: () => void;
  disabled: boolean;
  loading: boolean;
  children?: React.ReactNode;
};

export const RegisterPaper: React.FC<PaperProps> = (props) => {
  const theme = useTheme();
  return (
    <MUIPaper
      sx={{
        // ratio: 1,4
        height: "60%",
        width: "40%",
        boxShadow: 10,
      }}
    >
          <Grid container direction="row" justifyContent="flex-start" alignItems="space-evenly" sx={{ width: "100%", height: "100%"}}>
            <Grid item xs={12}>
              <Typography
                variant="h3"
                sx={{
                  fontWeight: theme.typography.fontWeightBold,
                  paddingLeft: 5,
                  paddingTop: 7
                }}
              >
                {"Registrieren"}
              </Typography>
            </Grid>
              <Grid item xs={5} sx={{ paddingLeft: 5, paddingBottom: 7 }}>
                <Box
                  sx={{
                    mt: 5,
                    mb: 2,
                  }}
                >
                  {props.loading ? <CircularProgress /> : props.children}
                </Box>
                <Button
                  variant="contained"
                  disabled={props.disabled}
                  onClick={props.handleAction}
                >
                  {"Registrieren"}
                </Button>
              </Grid>
            </Grid>
    </MUIPaper>
  );
};
