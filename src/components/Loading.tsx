import { CircularProgress, Grid } from "@mui/material";
import React from "react";

export class Loading extends React.Component<{}> {
    render(): React.ReactNode {
        return (
              <Grid
                container
                direction="column"
                justifyContent="center"
                alignItems="center"
              >
                <CircularProgress />
              </Grid>
          );
    }
}