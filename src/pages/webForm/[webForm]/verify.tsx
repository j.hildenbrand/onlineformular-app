import React, { useEffect, useState } from "react"
import { Box, CircularProgress, Grid, Paper, Typography } from "@mui/material"
import type { NextPage } from "next"
import { useRouter } from "next/router"
import { Loading } from "@/components/Loading"
import { useEmail } from "@/hooks/useEmail"
import { makeError } from "@/utils/error-handling/utils/makeError"
import { useVerify } from "@/hooks/useVerify"
import { useTheme } from "@mui/material/styles";
import MetaTitle from "@/components/MetaTitle"
import Header from "@/components/Header"


const VerifyEmailPage: NextPage = () => {
    const { sendEmail } = useEmail()
    const { verifyUser } = useVerify()
    const theme = useTheme();

    const [formId, setFormId] = useState<string>("")
    const [email, setEmail] = useState<string>("")
    const [id, setId] = useState<string>("")
    const [verified, setVerified] = useState<boolean | undefined>(undefined)
    const [alreadyVerified, setAlreadyVerified] = useState<boolean>(false)
    const router = useRouter()

    useEffect(() => {
        if (router.isReady) {
            const routerParams = router.query
            const tempEmail = routerParams["email"] as string
            const tempId = routerParams["id"] as string
            const tempFormId = routerParams["webForm"] as string
            if (email == undefined || id == undefined || tempFormId == undefined) {
                router.push("/404")
            }


            // call plugin for email verification

            setEmail(tempEmail)
            setId(tempId)
            setFormId(tempFormId)
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [router, router.isReady, router.query])

    const callSendEmail = async () => {

        const EmailResponse: any = await sendEmail(email, formId, id, "uniqueLinkEmailTemplate.html", "Formularlink für " + formId)
        if (EmailResponse.message != "Email sent successfully") {
          alert("Email Error: " + EmailResponse.message)
          return false
        }
        return true
      }

    useEffect(() => {
        if (email && formId && id) {
            try {
                verifyUser(id, formId, email)
                  .then((response: any) => {
                    if (response.message == "Verification successful") {
                        callSendEmail().then((emailResponse) => {setVerified(emailResponse)})
                    } else if (response.message == "User is already verified") {
                     
                      callSendEmail().then((emailResponse) => {setAlreadyVerified(emailResponse)})
                    } else {
                        alert("Verification Error: " + response.message)
                    }
                })
              } catch (error) {
                makeError(error).message, { variant: "error" };
              }
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [email, formId, id])

    if (alreadyVerified) {
      return (
        <>
        <MetaTitle title="Verification Success" />
        <Header title="Verification"  theme={theme}/>
        <Box
          sx={{
              width: "100%",
              height: "100vh",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#F7F7F2",
          }}>
          <Paper
            sx={{
              // ratio: 1,4
              height: "60%",
              width: "40%",
              boxShadow: 10,
            }}
          >
          <Grid container direction="row" justifyContent="flex-start" alignItems="space-evenly" sx={{ width: "100%", height: "100%"}}>
              <Grid item xs={12}>
                <Typography
                  variant="h3"
                  sx={{
                    fontWeight: theme.typography.fontWeightBold,
                    paddingLeft: 5,
                    paddingRight: 5,
                    paddingTop: 7
                  }}
                >
                 Ihre E-Mail ist bereits verifiziert. 
                </Typography>
                <Typography
                  variant="h6"
                  sx={{
                    fontWeight: theme.typography.fontWeightBold,
                    paddingLeft: 5,
                    paddingRight: 5,
                    paddingTop: 7
                  }}
                >
                 Bitte checken Sie Ihr Email Konto, da gibt es den Link zu Ihrer Bewerbung.
                </Typography>
              </Grid>
            </Grid>
          </Paper>
        </Box>
      </>
    )
    } else {
      switch (verified) {
        case true:
            return (
                <>
                <MetaTitle title="Verification Success" />
                <Header title="Verification"  theme={theme}/>
                <Box
                  sx={{
                      width: "100%",
                      height: "100vh",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      backgroundColor: "#F7F7F2",
                  }}>
                  <Paper
                    sx={{
                      // ratio: 1,4
                      height: "60%",
                      width: "40%",
                      boxShadow: 10,
                    }}
                  >
                  <Grid container direction="row" justifyContent="flex-start" alignItems="space-evenly" sx={{ width: "100%", height: "100%"}}>
                      <Grid item xs={12}>
                        <Typography
                          variant="h3"
                          sx={{
                            fontWeight: theme.typography.fontWeightBold,
                            paddingLeft: 5,
                            paddingRight: 5,
                            paddingTop: 7
                          }}
                        >
                         Ihre E-Mail wurde erfolgreich bestätigt
                        </Typography>
                        <Typography
                          variant="h6"
                          sx={{
                            fontWeight: theme.typography.fontWeightBold,
                            paddingLeft: 5,
                            paddingRight: 5,
                            paddingTop: 7
                          }}
                        >
                         Bitte checken Sie Ihr Email Konto, da gibt es den Link zu Ihrer Bewerbung.
                        </Typography>
                      </Grid>
                    </Grid>
                  </Paper>
                </Box>
              </>
            )
        case false:
            return (
                <>
                <MetaTitle title="Verification Failure" />
                <Header title="Verification"  theme={theme}/>
                <Box
                  sx={{
                      width: "100%",
                      height: "100vh",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      backgroundColor: "#F7F7F2",
                  }}>
                  <Paper
                    sx={{
                      // ratio: 1,4
                      height: "60%",
                      width: "40%",
                      boxShadow: 10,
                    }}
                  >
                  <Grid container direction="row" justifyContent="flex-start" alignItems="space-evenly" sx={{ width: "100%", height: "100%"}}>
                      <Grid item xs={12}>
                        <Typography
                          variant="h3"
                          sx={{
                            fontWeight: theme.typography.fontWeightBold,
                            paddingLeft: 5,
                            paddingRight: 5,
                            paddingTop: 7
                          }}
                        >
                         Ihre E-Mail konnte nicht erfolgreich bestätigt werden!
                        </Typography>
                        <Typography
                          variant="h6"
                          sx={{
                            fontWeight: theme.typography.fontWeightBold,
                            paddingLeft: 5,
                            paddingRight: 5,
                            paddingTop: 7
                          }}
                        >
                        Stellen Sie sicher bitte, dass Sie sich für das richtige Webformular registriert haben.
                        </Typography>
                      </Grid>
                    </Grid>
                  </Paper>
                </Box>
              </>
            )
        case undefined:
            return (
            <>
                <MetaTitle title="Verification Failure" />
                <Header title="Verification"  theme={theme}/>
                <Box
                  sx={{
                      width: "100%",
                      height: "100vh",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      backgroundColor: "#F7F7F2",
                  }}>
                  <Paper
                    sx={{
                      // ratio: 1,4
                      height: "60%",
                      width: "40%",
                      boxShadow: 10,
                    }}
                  >
                    <div style={{ paddingTop: "20%"}}></div>
                    <Loading />
                  </Paper>
                </Box>
            </>
          )
      }
    }
}

export default VerifyEmailPage

