import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Box, Button, Grid, Paper, TextField, Typography } from "@mui/material";
import type { NextPage } from "next";
import { useRouter } from "next/router";
import { makeError } from "@/utils/error-handling/utils/makeError";
import MetaTitle from "@/components/MetaTitle";
import { useRegistration } from "@/hooks/useRegistration";
import { useEmail } from "@/hooks/useEmail";
import Header from "@/components/Header";
import { useTheme } from "@mui/material/styles";

const Register: NextPage = () => {
  const router = useRouter();
  const { register } = useRegistration()
  const { sendEmail } = useEmail()
  const theme = useTheme();
  const [emailValid, setEmailValid] = useState<Error | true | null>(null);
  const [registeredSuccessfully, setRegisteredSuccessfully] = useState<boolean | null>(null);
  const [formId, setFormId] = useState<string>("")
  let email: string = ""

  const validateEmail = function (email: string): true | Error {
    const regex = new RegExp(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
    if (regex.test(email)) return true;
    else return new Error("Die Email ist nicht gültig!");
  };

  const error = useMemo(
    () => (router.query.error ? makeError(router.query.error) : null),
    [router.query.error]
  );

  useEffect(() => {
    if (router.isReady) {
      const routerParams = router.query
      setFormId(routerParams["webForm"] as string)
    }

  }, [router.isReady, router])

  const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value
    email = value
    if (value.length === 0) return setEmailValid(null)
    setEmailValid(validateEmail(value))
  }

  const callSendEmail = async (registerResponse: any, templateFile: string, subject: string) => {
    const EmailResponse: any = await sendEmail(email, formId, registerResponse.registrationData._id, templateFile, subject)
    if (EmailResponse.message != "Email sent successfully") {
      alert("Email Error: " + EmailResponse.message)
      return false
    }
    return true
  }

  const handleOnSubmit = useCallback(async () => {
    if (emailValid !== true || error != null) return;
    try {
      if (formId != "" && email != "") {
        const registerResponse: any = await register(email, formId)
        if (registerResponse.message == "User is already registered")  {
          if (registerResponse.alreadyVerified == true) {
            if (await callSendEmail(registerResponse, "uniqueLinkEmailTemplate.html", "Formularlink für " + formId )) {
              setRegisteredSuccessfully(false)
            }
          } else {
            if (await callSendEmail(registerResponse, "verifyEmailTemplate.html", "Bestätige deine E-Mail Adresse" )) {
              setRegisteredSuccessfully(false)
            }
          }
        } else if (registerResponse.message == "Registration successful") {
          if (await callSendEmail(registerResponse, "verifyEmailTemplate.html", "Bestätige deine E-Mail Adresse" )) {
            setRegisteredSuccessfully(true)
          }
        } else {
          alert("Registration Error: " + registerResponse.message)
        }
      }
    } catch (error) {
      makeError(error).message, { variant: "error" };
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [error, emailValid]);

  if (registeredSuccessfully == null) {
    return (
      <>
        <MetaTitle title="Registrieren" />
        <Header title="Registrieren"  theme={theme}/>
        <Box
          sx={{
              width: "100%",
              height: "100vh",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#F7F7F2",
          }}>
          <Paper
            sx={{
              // ratio: 1,4
              height: "60%",
              width: "40%",
              boxShadow: 10,
            }}
          >
          <Grid container direction="row" justifyContent="flex-start" alignItems="space-evenly" sx={{ width: "100%", height: "100%"}}>
            <Grid item xs={12}>
              <Typography
                variant="h4"
                sx={{
                  fontWeight: theme.typography.fontWeightBold,
                  paddingLeft: 5,
                  paddingRight: 5,
                  paddingTop: 7
                }}
              >
                {"Registrieren"}
              </Typography>
            </Grid>
              <Grid item xs={8} sx={{ paddingLeft: 5, paddingBottom: 7 }}>
                <Box
                  sx={{
                    mt: 5,
                    mb: 2,
                  }}
                >
                  <TextField
                    autoFocus
                    defaultValue={email}
                    onChange={handleOnChange}
                    label="E-Mail"
                    placeholder="your@email.com"
                    type="email"
                    required
                    error={emailValid instanceof Error}
                    helperText={emailValid instanceof Error ? emailValid.message : undefined}
                    fullWidth
                    variant="standard"
                />
                {error && <Typography sx={{ color: "red" }}>{error.message}</Typography>}
                </Box>
                <Button
                  variant="contained"
                  onClick={handleOnSubmit}
                >
                  {"Registrieren"}
                </Button>
              </Grid>
            </Grid>
          </Paper>
        </Box>
      </>
    )
  } else if (registeredSuccessfully == false) {
    return (
      <>
        <MetaTitle title="Registrieren Failure" />
        <Header title="Registrieren"  theme={theme}/>
        <Box
          sx={{
              width: "100%",
              height: "100vh",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#F7F7F2",
          }}>
          <Paper
            sx={{
              // ratio: 1,4
              height: "60%",
              width: "40%",
              boxShadow: 10,
            }}
          >
            <Grid container direction="row" justifyContent="flex-start" alignItems="space-evenly" sx={{ width: "100%", height: "100%"}}>
                <Grid item xs={12}>
                  <Typography
                    variant="h6"
                    sx={{
                      fontWeight: theme.typography.fontWeightBold,
                      paddingLeft: 5,
                      paddingRight: 5,
                      paddingTop: 7
                    }}
                  >
                    Die E-Mail wurde bereits benutzt, aber eine neue E-Mail mit Ihrem Bestätigungslink wurde gesendet!
                  </Typography>
                </Grid>
              </Grid>
            </Paper>
          </Box>
      </>
    )
  } else {
    return (
      <>
      <MetaTitle title="Registrieren Success" />
      <Header title="Registrieren"  theme={theme}/>
      <Box
        sx={{
            width: "100%",
            height: "100vh",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#F7F7F2",
        }}>
        <Paper
          sx={{
            // ratio: 1,4
            height: "60%",
            width: "40%",
            boxShadow: 10,
          }}
        >
        <Grid container direction="row" justifyContent="flex-start" alignItems="space-evenly" sx={{ width: "100%", height: "100%"}}>
            <Grid item xs={12}>
              <Typography
                variant="h4"
                sx={{
                  fontWeight: theme.typography.fontWeightBold,
                  paddingLeft: 5,
                  paddingRight: 5,
                  paddingTop: 7
                }}
              >
                Registrierung erfolgreich eingereicht
              </Typography>
            </Grid>
          </Grid>
        </Paper>
      </Box>
    </>
    )
  }
}

export default Register;
