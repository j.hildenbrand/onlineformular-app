import React, { useEffect, useState } from "react";
import {
  Box,
  Button,
  Grid,
  Paper,
  SelectChangeEvent,
  TextField,
  Typography,
  useTheme,
} from "@mui/material";
import {
  Input,
  Dropdown,
  File,
  Num,
  Date as UIDate,
  LargeInput,
  Submit,
} from "@/components/FormInputFields";
import { Loading } from "@/components/Loading";
import {
  Field,
  FormData,
  FormRequest,
  ColumnValuePair,
  UploadFileData,
} from "@/types/types";
import { useRouter } from "next/router";
import MetaTitle from "@/components/MetaTitle";
import { useWebform } from "@/hooks/useWebform";
import Header from "@/components/Header";
import { useHeibox } from "@/hooks/useHeibox";
import { hash } from "@/utils/hash";

const InputFormPage: React.FC = () => {
  const useForceUpdate = () => {
    //force re-render of the component
    const [value, setValue] = useState(0);
    return () => setValue((value) => value + 1);
  };

  const { requestWebform, submitWebform } = useWebform();
  const { uploadFile } = useHeibox();

  // const [workflowStep, setWorkflowStep] = useState<string | undefined>("");
  const [submitted, setSubmitted] = useState<boolean>(false);
  const [userId, setUserId] = useState<string | null>(null);
  const [formId, setFormId] = useState<string>("");
  const [webForm, setWebForm] = useState<FormRequest>();
  const [verified, setVerified] = useState<boolean | null>(null);
  const [workflowTrigger, setWorkflowTrigger] = useState<string | undefined>(
    ""
  );

  const forceUpdate = useForceUpdate();
  const router = useRouter();
  const theme = useTheme();

  useEffect(() => {
    if (router.isReady) {
      const routerParams = router.query;

      const tempUserId = routerParams["id"] as string;
      const tempFormId = routerParams["webForm"] as string;

      setFormId(tempFormId);

      requestWebform(tempUserId, tempFormId).then((response) => {
        if (response.message == "Applicant could not be found") {
          setUserId("")
          setWebForm(response.webForm)
        } else if (response.message == "Applicant is not yet verified") {
          setUserId("");
          setWebForm(response.webForm);
          setVerified(false);
        } else if (response.message == "Web form loaded successfully") {
          setUserId(tempUserId);
          setVerified(true);
          setWebForm(response.webForm);
          setWorkflowTrigger(response.webForm.workflowTrigger);
        } else {
          alert("Form Request Error: " + response.message);
        }
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router, router.isReady]);

  const checkRequired = () => {
    let checks = false;
    for (const field of webForm!.fields) {
      if (field.isRequired && (field.value == "%%%" || field.value == "")) {
        field.value = "";
        checks = true;
      }
    }
    return checks;
  };

  const getFile = async (fileString: string, fileName: string) => {
    let firstName = "id";
    let lastName = userId!;
    let email = userId!;
    for (const f of webForm!.fields) {
      if (f.fieldText == "Email") {
        email = f.value;
      }
      if (f.fieldText == "Vorname" || f.fieldText == "First Name") {
        firstName = f.value;
      }
      if (f.fieldText == "Nachname" || f.fieldText == "Name" || f.fieldText == "Last Name") {
        lastName = f.value;
      }
    }         

      const fileData: UploadFileData = {
          firstName: firstName,
          lastName: lastName,
          emailHash: hash(email),
          file: fileString,
          fileName: fileName,
      }
      return await uploadFile(fileData)
  }

  const createFormData = async () => {
    let formData: FormData = {_id: userId!, workflowTrigger, form: []};
    for (const field of webForm!.fields) {
      let fieldData: ColumnValuePair;
      if (field.fieldType == "File" && (field.fileName != "%%%" && field.fileName != "")) {
        const file = field.value.files[0];
        let fileString: string = ""
        const reader = new FileReader()
        reader.readAsDataURL(file)

        reader.onload = async () => {
            if (typeof reader.result !== "string") {
                throw new Error("reader.result is not a string")
            }
            fileString = reader.result
            const response: any = await getFile(fileString, file.name)
            if (response.message != "uploadFile succeeded.") {
              alert("There was an error uploading your file. Please email your document to the Dekanat's Office for further processing.")
              console.log("There was an error uploading your file. Please email your document to the Dekanat's Office for further processing.")

            } else {
              fieldData = {
                tableId: field.table,
                input:  response.link ? response.link : "",
                columnId: field.column,
              };
              formData.form.push(fieldData)

            }
        }
        reader.onerror = () => {
          console.log(reader.error)
        }
      } else {
        if (
          field.value != null &&
          field.value != undefined &&
          field.value != "%%%"
          ) {
          fieldData = {
            tableId: field.table,
            input: field.value,
            columnId: field.column,
          };
          formData.form.push(fieldData)
        }
      }
    }
    return formData;
  };

  const handleSubmit = async () => {
    if (checkRequired()) {
      forceUpdate();
      alert("Bitte füllen Sie alle Pflichtfelder aus");
      console.log("Bitte füllen Sie alle Pflichtfelder aus");
      return;
    }
   const formData = await createFormData()
   await new Promise(f => setTimeout(f, 3000));
    const response: any = await submitWebform(formData);
    if (response.message != "Submitted Successfully") {
      alert("Form Submission Error: " + response.message)
      return
    }

    forceUpdate();
    setSubmitted(true);
  };

  const renderField = (field: Field) => {
    switch (field["fieldType"]) {
      case "Input":
        return (
          <Input
            field={field}
            error={field.isRequired == true && field.value == "" ? true : false}
            defaultVal={field.value != "%%%" ? (field.value as string) : ""}
            onChange={(event: SelectChangeEvent) => {
              field.value = event.target.value;
              forceUpdate();
            }}
          />
        );
      case "File":
        return (
          <Grid
            container
            direction="row"
            justifyContent="space-between"
            alignItems="center"
          >
            <Grid container item xs={8}>
              <File
                field={field}
                error={
                  field.isRequired == true && field.value == "" ? true : false
                }
                onChange={(event: SelectChangeEvent) => {

                  field.value = event.target
                  field.fileName = field.value.files[0].name
                  forceUpdate();
                }}
              />
            </Grid>
            <Grid container item xs={4} justifyContent="flex-end">
              <Typography variant="body2" color="text.secondary">
                {field.fileName != "%%%" ? (field.fileName as string) : ""}
              </Typography>
            </Grid>
          </Grid>
        );
      case "Dropdown":
        return (
          <Dropdown
            field={field}
            error={field.isRequired == true && field.value == "" ? true : false}
            defaultVal={field.value != "%%%" ? (field.value as string) : ""}
            onChange={(event: SelectChangeEvent) => {
              field.value = event.target.value;
              forceUpdate();
            }}
          />
        );
      case "Number":
        return (
          <Num
            field={field}
            error={field.isRequired == true && field.value == "" ? true : false}
            defaultVal={field.value != "%%%" ? (field.value as string) : ""}
            onChange={(event: SelectChangeEvent) => {
              field.value = event.target.value;
              forceUpdate();
            }}
          />
        );
      case "Date":
        return (
          <UIDate
            field={field}
            error={field.isRequired == true && field.value == "" ? true : false}
            defaultVal={field.value != "%%%" ? (field.value as string) : ""}
            onChange={(event: SelectChangeEvent) => {
              field.value = event.target.value;
              forceUpdate();
            }}
          />
        );
      case "Large Input":
        return (
          <LargeInput
            field={field}
            error={field.isRequired == true && field.value == "" ? true : false}
            defaultVal={field.value != "%%%" ? (field.value as string) : ""}
            onChange={(event: SelectChangeEvent) => {
              field.value = event.target.value;
              forceUpdate();
            }}
          />
        );
      case "Text":
        return (
          <Grid
            container
            item
            direction="row"
            justifyContent="space-between"
            alignItems="center"
            xs={11.5}
            sx={{ marginTop: "10px", marginBottom: "10px" }}
          >
            <Typography
              variant="body1"
              color="black.500"
              sx={{ padding: "3px", fontWeight: "bold" }}
            >
              {field.fieldText}
            </Typography>
          </Grid>
        );
    }
  };

  if (webForm! && submitted) {
    return (
      <>
        <MetaTitle title={"Submission Complete"} />
        <Header title={webForm.formTitle} theme={theme} />
        <Box
          sx={{
            width: "100%",
            height: "100vh",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#F7F7F2",
          }}
        >
          <Grid
            xs={12}
            container
            justifyContent="center"
            alignItems="center"
            direction="column"
            sx={{ width: "100%", height: "100%" }}
          >
            <div style={{ height: "8%" }}></div>
            <Paper
              elevation={5}
              sx={{ width: "75%", backgroundColor: "#FFFFFF", height: "88%" }}
            >
              <Grid
                container
                sx={{ padding: 5, height: "100%" }}
                justifyContent="space-between"
              >
                <Grid container item xs={12} justifyContent="space-between">
                  <Grid container item xs={9} direction="column">
                    <span style={{ ...theme.typography.h4 }}>
                      {webForm.formTitle}
                    </span>
                    <span style={{ ...theme.typography.h6, paddingTop: "3px" }}>
                      {webForm.formDescription}
                    </span>
                  </Grid>
                </Grid>
                <Grid container item xs={12}>
                  <Typography
                    variant="body1"
                    color="black.500"
                    sx={{ padding: "3px", fontWeight: "bold" }}
                  >
                    Vielen Dank für Ihren Beitrag!
                  </Typography>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        </Box>
      </>
    );
  } else if (userId != null && webForm!) {
    if (userId == "" && verified == false) {
      return (
        <>
          <MetaTitle title={webForm.formTitle} />
          <Header title={webForm.formTitle} theme={theme} />
          <Box
            sx={{
              width: "100%",
              height: "100vh",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#F7F7F2",
            }}
          >
            <Grid
              xs={12}
              container
              justifyContent="center"
              alignItems="center"
              direction="column"
              sx={{ width: "100%", height: "100%" }}
            >
              <div style={{ height: "8%" }}></div>
              <Paper
                elevation={5}
                sx={{ width: "75%", backgroundColor: "#FFFFFF", height: "88%" }}
              >
                <Grid
                  container
                  sx={{ padding: 5, height: "100%" }}
                  justifyContent="space-between"
                >
                  <Grid container item xs={12} justifyContent="space-between">
                    <span style={{ ...theme.typography.h4 }}>
                      {webForm.formTitle}
                    </span>
                  </Grid>
                  <Grid container item xs={12}>
                    <Typography
                      variant="body1"
                      color="black.500"
                      sx={{ padding: "3px", fontWeight: "bold" }}
                    >
                      Ihre E-Mail wurde noch nicht verifiziert. Bitte überprüfen
                      Sie Ihre E-Mail, um den Verifizierungsschritt
                      abzuschließen.
                    </Typography>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
          </Box>
        </>
      );
    } else if (userId == "" && verified == null) {
      return (
        <>
          <MetaTitle title={webForm.formTitle} />
          <Header title={webForm.formTitle} theme={theme} />
          <Box
            sx={{
              width: "100%",
              height: "100vh",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#F7F7F2",
            }}
          >
            <Grid
              xs={12}
              container
              justifyContent="center"
              alignItems="center"
              direction="column"
              sx={{ width: "100%", height: "100%" }}
            >
              <div style={{ height: "8%" }}></div>
              <Paper
                elevation={5}
                sx={{ width: "75%", backgroundColor: "#FFFFFF", height: "88%" }}
              >
                <Grid
                  container
                  sx={{ padding: 5, height: "100%" }}
                  justifyContent="space-between"
                >
                  <Grid container item xs={12} justifyContent="space-between">
                    <Grid container item xs={9} direction="column">
                      <span style={{ ...theme.typography.h4 }}>
                        {webForm.formTitle}
                      </span>
                      <span
                        style={{ ...theme.typography.h6, paddingTop: "3px" }}
                      >
                        {webForm.formDescription}
                      </span>
                    </Grid>
                  </Grid>
                  <Grid container item xs={12}>
                    <Typography
                      variant="body1"
                      color="black.500"
                      sx={{ padding: "3px", fontWeight: "bold" }}
                    >
                      Ihre Bewerbung ist nicht in unserem System vorhanden.
                      Bitte registrieren Sie sich für eine neue Bewerbung.
                    </Typography>
                  </Grid>
                  <Grid container item xs={12}>
                    <Button
                      variant="contained"
                      component="label"
                      sx={{ marginTop: "10px", width: "auto" }}
                      onClick={() => {
                        router.push("/webForm/" + formId + "/register");
                      }}
                    >
                      Registrieren
                    </Button>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
          </Box>
        </>
      );
    } else {
      return (
        <>
          <MetaTitle title={webForm.formTitle} />
          <Header title={webForm.formTitle} theme={theme} />
          <Box
            sx={{
              width: "100%",
              height: "100vh",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#F7F7F2",
            }}
          >
            <Grid
              xs={12}
              container
              justifyContent="center"
              alignItems="center"
              direction="column"
              sx={{ width: "100%", height: "100%" }}
            >
              <div style={{ height: "8%" }}></div>
              <Paper
                elevation={5}
                sx={{ width: "75%", backgroundColor: "#FFFFFF", height: "88%" }}
              >
                <Grid
                  container
                  sx={{ padding: 5, height: "100%" }}
                  justifyContent="space-between"
                >
                  <Grid container item xs={12} justifyContent="space-between">
                    <Grid container item xs={9} direction="column">
                      <span style={{ ...theme.typography.h4 }}>
                        {webForm.formTitle}
                      </span>
                      <span
                        style={{ ...theme.typography.h6, paddingTop: "3px" }}
                      >
                        {webForm.formDescription}
                      </span>
                    </Grid>
                    <Grid container item justifyContent="flex-end" xs={3}>
                      <TextField
                        disabled
                        focused
                        multiline
                        fullWidth
                        variant="filled"
                        value={userId}
                        label="Nutzer ID"
                        helperText="Speichern Sie diese ID, um später auf diese Bewerbung zuzugreifen"
                        InputLabelProps={{ shrink: true }}
                      />
                    </Grid>
                  </Grid>
                  <Grid item container xs={12} direction="row" justifyContent="center" alignItems="center">
                  {webForm.fields.length != 0 && (
                    <>
                      {webForm.fields.map((field: Field, i: number) => (
                        <Grid item xs={12} key={i}>
                          {renderField(field)}
                        </Grid>
                      ))}
                      {!webForm.onlyText && (
                        <>
                          <Grid item xs={12}>
                            <Typography
                              style={{
                                ...theme.typography.body2,
                                paddingTop: "5px",
                                color: "gray",
                                fontSize: "10px",
                                
                              }}
                            >
                              * Pflichtfelder
                            </Typography>
                          </Grid>

                            <Grid
                              item
                              container
                              xs={12}
                              direction="row"
                              justifyContent="flex-end"
                              alignItems="center"
                            >
                              <Submit theme={theme} onClick={handleSubmit} />
                            </Grid>
                          </>
                        )}
                      </>
                    )}

                    {webForm!.fields.length == 0 && <>{Loading}</>}
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
          </Box>
        </>
      );
    }
  } else {
    return (
      <>
        <MetaTitle title={"Loading..."} />
        <Header title={"Onlineformular"} theme={theme} />
        <Box
          sx={{
            width: "100%",
            height: "100vh",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#F7F7F2",
          }}
        >
          <Grid
            xs={12}
            container
            justifyContent="center"
            alignItems="center"
            direction="column"
            sx={{ width: "100%", height: "100%" }}
          >
            <div style={{ height: "8%" }}></div>
            <Loading />
          </Grid>
        </Box>
      </>
    );
  }
};

export default InputFormPage;
