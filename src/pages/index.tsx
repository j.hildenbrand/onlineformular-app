import React, { useEffect, useState } from "react"
import { Box, Card, CardContent, Grid, Paper, Typography } from "@mui/material"
import type { NextPage } from "next"
import { Loading } from "@/components/Loading"
import { useTheme } from "@mui/material/styles";
import { useForms } from "@/hooks/useForms"
import router from "next/router"
import { FormInformation } from "@/types/types"
import Header from "@/components/Header";
import MetaTitle from "@/components/MetaTitle";

const VerifyEmailPage: NextPage = () => {

    const { getForms } = useForms()
    const theme = useTheme();
    const [ allForms, setAllForms ] = useState<any>([])

    useEffect(() => {
        getForms().then((response: any) => {
            setAllForms(response.formInformation)
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    if (!allForms) {
        return (
            <>
            <MetaTitle title="Onlineformular Portal" />
            <Header title="Onlineformular" theme={theme}/>
            <Box
              sx={{
                  width: "100%",
                  height: "100vh",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "#F7F7F2",
              }}>
                <Paper
                    sx={{
                    // ratio: 1,4
                    height: "60%",
                    width: "40%",
                    boxShadow: 10,
                    }}
                >
                    <Grid container direction="row" justifyContent="flex-start" alignItems="space-evenly" sx={{ width: "100%", height: "100%"}}>
                        <div style={{ paddingTop: "20%"}}></div>

                        <Loading />
                    </Grid>
                </Paper>
            </Box>
        </>
        )
    } else {
        return (
            <>
                <MetaTitle title="Onlineformular Portal" />
                <Header title="Onlineformular"  theme={theme}/>
                <Box
                  sx={{
                      width: "100%",
                      height: "100vh",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      backgroundColor: "#F7F7F2",
                  }}>
                  <Paper
                    sx={{
                      // ratio: 1,4
                      height: "60%",
                      width: "40%",
                      boxShadow: 10,
                    }}
                  >
                    <Grid container direction="row" justifyContent="flex-start" alignItems="space-evenly" sx={{ width: "100%", height: "100%"}}>

                        <Grid item xs={12}>
                            <Typography
                                variant="h5"
                                sx={{
                                fontWeight: theme.typography.fontWeightBold,
                                paddingLeft: 5,
                                paddingRight: 5,
                                paddingTop: 7
                                }}
                            >
                                Bitte wählen Sie das Formular, für das Sie sich anmelden möchten:
                            </Typography>
                        </Grid>
                    <Grid container item justifyContent="center" spacing={2}>
                        {allForms.map((form: FormInformation, i: number) => (
                            <Grid item key={i} sx={{ padding: 2 }}>
                                <Card
                                    onClick={() => {
                                        router.push(`/webForm/${form.formId}/register`)
                                    }}
                                    sx={{
                                        minWidth: 150,
                                        minHeight: 150,
                                        cursor: "pointer",
                                        display: "flex",
                                        justifyContent: "center",
                                        alignItems: "center",
                                        boxShadow: 3,
                                        borderRadius: 2,
                                        marginBottom: 2,
                                    }}
                                >
                                    <CardContent sx={{fontFamily: '"Segoe UI"', fontWeight: "medium", fontSize: "15px"}}>
                                        {form.formTitle}
                                    </CardContent>
                                </Card>
                            </Grid>
                        ))}
                        </Grid>
                    </Grid>
                </Paper>
            </Box>
        </>
        )
    }
}

export default VerifyEmailPage

