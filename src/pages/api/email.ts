import type { NextApiRequest, NextApiResponse } from "next";

export const sendEmail = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {


  const loginResponse = await fetch("http://localhost:8080/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    redirect: "manual",
    credentials: "include",
    body: `username=admin@dekanat.de&password=password`,
  });


  if (loginResponse.status !== 302)
    throw new Error(`Netzwerkfehler, Status = ${loginResponse.status}`);

  const text = await loginResponse.text();

  if (text.includes("secret") === false)
    throw new Error("Kombination aus Nutzername und Passwort nicht gefunden!");

  const cookie = loginResponse.headers
    .get("set-cookie")!
    .split(";")
    .map((c) => c.split("="))
    .find((c) => c[0] === "connect.sid")![1];
  /********************************************************************************* */
  const response = await fetch(
    "http://localhost:8080/request/web-forms/sendEmail",
    {
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        ...(cookie && {
          Cookie: "connect.sid" + "=" + cookie,
        }),
      },
      credentials: "include",
      redirect: "manual",
      body: JSON.stringify(req.body),
    }
  );
  /********************************************************************************* */
  await fetch("http://localhost:8080/logout", {
    method: "POST",
    credentials: "include",
    headers: {
      ...(cookie && {
        Cookie: "connect.sid" + "=" + cookie,
      }),
    },
  });

  const jsonBody = await response.json();
  console.dir(req.body);
  console.dir(jsonBody);
  res.status(200).send(jsonBody);
  return;
};

// eslint-disable-next-line import/no-anonymous-default-export
export default async (req: NextApiRequest, res: NextApiResponse) => {
  switch (req.method) {
    case "POST":
      await sendEmail(req, res);
      break;
    default:
      res
        .status(["HEAD", "GET"].includes(req.method!) ? 500 : 501)
        .send("This method is not supported!");
  }
};
