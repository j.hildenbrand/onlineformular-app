import type { NextApiRequest, NextApiResponse } from "next";

export const getAllForms = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const loginResponse = await fetch("http://localhost:8080/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      // "Content-Type": "application/json",
    },
    redirect: "manual",
    credentials: "include",
    body: `username=admin@dekanat.de&password=password`,
  });

  console.dir(loginResponse);

  if (loginResponse.status !== 302)
    throw new Error(`Netzwerkfehler, Status = ${loginResponse.status}`);

  const text = await loginResponse.text();

  if (text.includes("secret") === false)
    throw new Error("Kombination aus Nutzername und Passwort nicht gefunden!");

  const cookie = loginResponse.headers
    .get("set-cookie")!
    .split(";")
    .map((c) => c.split("="))
    .find((c) => c[0] === "connect.sid")![1];
  /********************************************************************************* */
  const response = await fetch(
    "http://localhost:8080/request/web-forms/getAllForms",
    {
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        ...(cookie && {
          Cookie: "connect.sid" + "=" + cookie,
        }),
      },
      credentials: "include",
      redirect: "manual",
    }
  );
  /********************************************************************************* */
  await fetch("http://localhost:8080/logout", {
    method: "POST",
    credentials: "include",
    headers: {
      ...(cookie && {
        Cookie: "connect.sid" + "=" + cookie,
      }),
    },
  });

  const jsonBody = await response.json();

  res.status(200).send(jsonBody);
  return;
};

// eslint-disable-next-line import/no-anonymous-default-export
export default async (req: NextApiRequest, res: NextApiResponse) => {
  switch (req.method) {
    case "GET":
      await getAllForms(req, res);
      break;
    default:
      res
        .status(["HEAD", "POST", "PATCH"].includes(req.method!) ? 500 : 501)
        .send("This method is not supported!");
  }
};
