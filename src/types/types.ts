export interface UploadFileData {
  firstName: string;
  lastName: string;
  emailHash: number;
  file: string; //base64 string
  fileName: string;
}

export type EmailData = {
  id: string;
  email: string;
};

//submitWebform
export type FormData = {
  _id: string;
  workflowTrigger?: string;
  form: ColumnValuePair[];
};

export type ColumnValuePair = {
  tableId: string;
  input: any;
  columnId: string;
};

//requestWebform
export type FormRequest = {
  formTitle: string;
  formDescription: string;
  workflowTrigger?: string;
  fields: Field[];
  onlyText: boolean;
};

export interface Field {
  fieldText: string
  fieldType: string
  table: string
  column: string
  fieldDescription?: string
  isRequired?: boolean
  renderValue?: boolean
  values?: string[]
  fileName?: string
  value?: any
}

export type FormInformation = {
  formId: string;
  formTitle: string;
};
