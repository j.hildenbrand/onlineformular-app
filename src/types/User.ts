export type User = {
    username: string
    authCookie: string
    id: number
    isLoggedIn: boolean
    role: Role
}

export declare type Role = {
    id: number;
    roleKind?: RoleKind;
    name: string;
    description: string;
};
export declare enum RoleKind {
    /** Full Privileges. */
    Admin = 0,
    /** No privileges, not even viewing tables. */
    Guest = 1
}