import { fetcher } from "@/api/fetcher";
import { FormInformation } from "@/types/types";

export const useForms = () => {
  const getForms = async (): Promise<FormInformation> => {
    return await fetcher({
      url: "/api/forms",
      method: "GET",
    });
  };

  return {
    getForms,
  };
};
