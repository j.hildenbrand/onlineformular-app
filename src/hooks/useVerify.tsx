import { fetcher } from "@/api";

export const useVerify = () => {
    const verifyUser = async (
        userId: string,
        formId: string,
        email: string
      ): Promise<void> => {
        return await fetcher({
          url: "/api/verify",
          body: {
            userId: userId,
            formId: formId,
            email: email,
          },
          method: "POST",
        });
      };
  
 

  return {
    verifyUser
  };
};
