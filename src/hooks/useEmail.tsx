import { fetcher } from "@/api";

export const useEmail = () => {
  const sendEmail = async (
    email: string,
    formId: string,
    id: string,
    templateFile: string,
    subject: string
  ): Promise<void> => {
    return await fetcher({
      url: "/api/email",
      method: "POST",
      body: {
        email,
        formId,
        id,
        templateFile,
        subject,
      },
    });
  };
  
 

  return {
    sendEmail,
  };
};
