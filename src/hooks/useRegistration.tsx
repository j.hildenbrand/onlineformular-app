import { fetcher } from "@/api/fetcher";

export const useRegistration = () => {
  const register = async (email: string, formId: string): Promise<void> => {
    return await fetcher({
      url: "/api/registration",
      body: {
        email: email,
        formId: formId,
      },
      method: "POST",
    });
  };

  return {
    register,
  };
};
