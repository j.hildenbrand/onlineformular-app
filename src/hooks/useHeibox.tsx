import { fetcher } from "@/api"
import { UploadFileData } from "@/types/types"

export type HeiboxDescriptor = { isItChristmas: boolean };

export const useHeibox = () => {
  const uploadFile = async (fileData: UploadFileData) => {
    return await fetcher({
      url: "/api/heibox/upload-file",
      body: { fileData: fileData },
      method: "POST",
    });
  };

  return {
    uploadFile,
  };
};
