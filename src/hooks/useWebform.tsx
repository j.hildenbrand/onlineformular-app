import { fetcher } from "@/api/fetcher";
import { FormData } from "@/types/types";

export const useWebform = () => {
  const requestWebform = async (
    userId: string,
    formId: string
  ): Promise<any> => {
    return await fetcher({
      url: "/api/webForm",
      body: {
        method: "requestWebform",
        userId: userId,
        formId: formId,
      },
      method: "POST",
    });
  };

  const submitWebform = async (submittedData: FormData): Promise<void> => {
    return await fetcher({
      url: "/api/webForm",
      body: {
        method: "submitWebform",
        submittedData: submittedData,
      },
      method: "POST",
    });
  };

  return {
    requestWebform,
    submitWebform,
  };
};
