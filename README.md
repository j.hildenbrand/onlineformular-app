# Onlineformular-app

## Description

Frontend application for registration, verification, and submitting to different web forms within the [dekanat-app](https://gitlab.com/pvs-hd-teaching/2022-ws/dekanat-app/-/tree/feat/f1000/web-forms)

## What Does This App Do?

onlineformular-app is a simple and efficent configurable web form portal front end for the dekanat-app. Through the simple UI, users can register and submit data for different web forms that are within the dekanat-app. This also includes verification steps for users to register, as well as emails to distribute secure, automatically generated links for 2-step verification and web form access. In order to fully function, this application interacts with 4 other repositories:

- [intutable/web-forms](https://gitlab.com/intutable/web-forms)
- [intutable/seafile-client](https://gitlab.com/intutable/seafile-client)
- [intutable/Process Manager](https://gitlab.com/intutable/process-manager)
- [dekanat-app](https://gitlab.com/pvs-hd-teaching/2022-ws/dekanat-app/-/tree/feat/f1000/web-forms)

All intutable repositories are plugins for the dekanat-app which need to be properly installed.

## Getting Started

### Installation

1. Clone the repo
2. Clone the [dekanat-app](https://gitlab.com/pvs-hd-teaching/2022-ws/dekanat-app/-/tree/feat/f1000/web-forms) repo
3. Clone the [intutable/web-forms](https://gitlab.com/intutable/web-forms) repo
4. Clone the [intutable/seafile-client](https://gitlab.com/intutable/seafile-client) repo
5. Clone the [intutable/Process Manager](https://gitlab.com/intutable/process-manager) repo
6. `cd web-forms`
7. `npm install`
8. `npm run build`
9. `cd seafile-client`
10. `npm install`
11. `npm run build`
12. `cd Process Manager`
13. `npm install`
14. `npm run build`
15. `cd onlineformular-app`
16. `npm install`
17. - Dev mode:
      `npm run build` (only needed once)  
      `npm run dev`


    - Production mode:
      `npm run build`
      `npm run start`
    - `npm run reset -w database` resets the database

18. Open a seperate terminal
19. `cd dekanat-app`
20. `npm install`
21. - Dev mode:
      `npm run build` (only needed once)  
      `npm run dev`


    - Production mode:
      `npm run build`
      `npm run start`
    - `npm run reset -w database` resets the database

Folder Structure:\
.\
├── dekanat-app\
├── onlineformular-app\
├── Process Manager\
├── seafile-client\
├── web-forms\

### Usage

##### NOTE: Ensure that onlineformular-app is currently running on `http://localhost:3000` in the browser.

After starting the app, navigate to its location at `http://localhost:3000` in the browser. In dev mode, a user can click on any of the available web forms and continue to register for them. Entering any email will work and the user will be required to verify their email, and then continue to verify your email, and finally navigate to the form.

## Documentation

There is a high-level overview of the application's structure in /docs/dev.
